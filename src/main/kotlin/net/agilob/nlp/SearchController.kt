package net.agilob.nlp

import org.springframework.social.twitter.api.Twitter
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import javax.inject.Inject

@RestController
class SearchController @Inject constructor(mTwitter: Twitter, mNlpService: NlpService) {

    private var twitter: Twitter = mTwitter;

    private var nlpService: NlpService = mNlpService;

    @GetMapping("/")
    fun index() : String {
        return "Hello world"
    }

    @GetMapping("/{hashtag}")
    fun queryTwitter(@PathVariable hashtag: String) : List<String> {
        var results = mutableListOf<String>()
        var searchResult = twitter.searchOperations().search("q:#" + hashtag)
        searchResult.tweets.forEach {
            var sentiment = nlpService.findSentiment(it.text)
            println("Analysing: " + it.text)
            results.add(sentiment.toString() + "::" + it.text)
        }

        return results;
    }
}