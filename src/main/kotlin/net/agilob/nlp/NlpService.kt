package net.agilob.nlp

import edu.stanford.nlp.ling.CoreAnnotations
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class NlpService {

  private lateinit var pipeline: StanfordCoreNLP;

  @PostConstruct
  fun init() {
    pipeline = StanfordCoreNLP("nlp.properties");
  }

  fun findSentiment(tweetData: String): Int {
    var mainSentiment = 0
    var cleanTweet  = cleanTweet(tweetData)
    if (cleanTweet.isNotEmpty()) {
     var longest = 0
      var annotation = pipeline.process(cleanTweet)

      annotation.get(CoreAnnotations.SentencesAnnotation::class.java).forEach {
        var tree = it.get(SentimentCoreAnnotations.SentimentAnnotatedTree::class.java)
        var sentiment = RNNCoreAnnotations.getPredictedClass(tree)
        var partText = it.toString()
        if (partText.length > longest) {
          mainSentiment = sentiment
          longest = partText.length;
        }
      }
    }
    return mainSentiment;
  }

  fun cleanTweet(tweetData: String): String {
    return tweetData.toLowerCase()
            .replace("(@[A-Za-z0-9_]+)|([^0-9A-Za-z \\t])|(\\w+:\\/\\/\\S+)", " ")
            .replace("https.*", "")
  }

}
